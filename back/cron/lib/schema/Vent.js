
const moment = require('moment');
const cToF = require('celsius-to-fahrenheit');

const Thresh = require('../thresholds');

const AWS = require('aws-sdk');
AWS.config.update({region: process.env.REGION || 'us-west-2'});
const doc = require('dynamodb-doc');
const Dynamo = new doc.DynamoDB();

var SUFFIX = process.env.SUFFIX || '-pilot';

module.exports = class Vent {

  constructor(vent){
    this.vId = vent.id;
    this.uId = vent.uId;
    this.bId = vent.bId;
    this.currentFilter = this.getCurrentFilter(vent);
  }

  getCurrentFilter(vent){
    if (!vent.airFilters || vent.airFilters.length < 1) {
      return null;
    } else {
      let filter = vent.airFilters.find(f => !f.toTimestamp);
      if (filter === undefined) {
        return null;
      } else {
        return {
          fId: filter.id,
          changed: filter.fromTimestamp
        }
      }
    }
  }

  getDailyAvgs(days){

    return new Promise(async resolve => {
      var range = await Promise.all(days.map(async d => {
        d.meas = await this.getMeasForDay(d);
        return d;
      }));
      range = await Promise.all(days.map(async d => {
        d.daps = await this.getDapForDay(d);
        return d;
      }))
      this.range = range;
      resolve(this);
    });

  }

  getMeasForDay(range){
    var params = {
      TableName: 'Measurements' + SUFFIX,
      KeyConditionExpression: 'did = :dId AND tms BETWEEN :start AND :end',
      ExpressionAttributeValues: {
        ':dId': this.device.dId,
        ':start': range.start,
        ':end': range.end
      },
      ScanIndexForward: false
    }
    return new Promise((resolve, reject) => {
      Dynamo.query(params, (err, data) => {
        if (err) {
          resolve(null);
        } else {
          if (data.Items.length < 1) {
            resolve(null);
          } else {
            var meas = data.Items;
            meas = refineMeas(meas);
            resolve(meas);
          }
        }
      })
    });
  }

  getDapForDay(day){
    var params = {
      TableName: 'DataAnalysisPackets' + SUFFIX,
      KeyConditionExpression: 'dataAnalysisPK = :dap AND #tms BETWEEN :start and :end',
      ExpressionAttributeNames: {
        '#tms': 'timestamp'
      },
      ExpressionAttributeValues: {
        ':dap': this.currentFilter.fId + '.DAILY_AIR_FILTER',
        ':start': day.start,
        ':end': day.end
      },
      ScanIndexForward: false
    }
    return new Promise((resolve, reject) => {
      Dynamo.query(params, (err, data) => {
        if (err || data.Items.length < 1) {
          resolve(null);
        } else {
          var daysRem = data.Items;
          daysRem = refineDays(daysRem);
          resolve(daysRem);
        }
      });
    })
  }

  aggregateWeek(){
    var week = this.range.reduce((a, i) => {
      if (i.meas === null) {
        return a;
      } else {
        a.daysWithMeas += 1;
        for (let prop in i.meas) {
          a[prop].avg += i.meas[prop].avg;
          for (let pProp in i.meas[prop].percentages) {
            if (!a[prop].percentages[pProp]) {
              a[prop].percentages[pProp] = i.meas[prop].percentages[pProp];
            } else {
              a[prop].percentages[pProp] += i.meas[prop].percentages[pProp];
            }
          }
        }
      }

      if (i.daps === null) {
        return a;
      } else {
        a.daysWithDaps += 1;
        if (a.daps.avg === null) {
          a.daps.avg = i.daps.avg;
        } else {
          a.daps.avg += i.daps.avg;
        }
        return a;
      }
    }, {
      daysWithMeas: 0,
      daysWithDaps: 0,
      aq: {
        avg: 0, tier: '', units: 'iaq', percentages: {}
      },
      run: {
        avg: 0, tier: '', units: '%', percentages: {}
      },
      temp: {
        avg: 0, tier: '', units: '˚F', percentages: {}
      },
      hum: {
        avg: 0, tier: '', units: '%', percentages: {}
      },
      daps: {
        avg: null, tier: '', units: 'days', percentages: {}
      }
    });
    delete week.daps.percentages;
    delete week.run.percentages;
    for (let prop in week) {
      if (prop !== 'daysWithMeas' && prop !== 'daysWithDaps') {
        if (week[prop].avg !== null) {
          var length = prop === 'dap' ? week.daysWithDaps : week.daysWithMeas;
          week[prop].avg = Math.round(week[prop].avg / length);
          if (week[prop].percentages) {
            for (let pProp in week[prop].percentages) {
              week[prop].percentages[pProp] = Math.round(week[prop].percentages[pProp] / week.daysWithMeas);
            }
          }
        }
      }
    }
    week.aq.tier = setThreshold(week.aq.avg, Thresh.aqThresh) || null;
    week.run.tier = setThreshold(week.run.avg, Thresh.runThresh) || null;
    week.temp.tier = setThreshold(week.temp.avg, Thresh.tempThresh) || null;
    week.hum.tier = setThreshold(week.hum.avg, Thresh.humThresh) || null;
    week.daps.tier = setThreshold(week.daps.avg, Thresh.daysThresh) || null;
    this.week = week;
    return this;
  }

}


function refineMeas(meas){
  var aqVals = [];
  var runVals = [];
  var tempVals = [];
  var humVals = [];
  meas.map(m => {
    aqVals = aqVals.concat(m.dat.iaq);
    runVals = runVals.concat(m.dat.run);
    tempVals = tempVals.concat(m.dat.ttp.map(t => cToF(t)));
    humVals = tempVals.concat(m.dat.hhm);
  });
  var avgs = {
    aq: getAvg(aqVals),
    temp: getAvg(tempVals),
    hum: getAvg(humVals)
  }

  var ons = runVals.filter(r => r === 1);
  avgs.run = Math.round((ons.length/runVals.length) * 100);

  avgs.aq = {avg: avgs.aq, tier: setThreshold(avgs.aq, Thresh.aqThresh), unit: 'iaq'};
  avgs.run = {avg: avgs.run, tier: setThreshold(avgs.run, Thresh.runThresh), unit: '%'}
  avgs.temp = {avg: avgs.temp, tier: setThreshold(avgs.temp, Thresh.tempThresh), unit: '˚F'};
  avgs.hum = {avg: avgs.hum, tier: setThreshold(avgs.hum, Thresh.humThresh), unit: '%'}

  avgs.aq.percentages = getPercentages(aqVals, Thresh.aqThresh);
  avgs.temp.percentages = getPercentages(tempVals, Thresh.tempThresh);
  avgs.hum.percentages = getPercentages(humVals, Thresh.humThresh);

  return avgs;

}

function refineDays(days){
  var daysVals = days.map(d => {
    if (d.values && d.values.lifeExpectancy) {
      var daysRem = d.values.lifeExpectancy;
      if (daysRem === -2) daysRem = 180;
      if (daysRem === -1) daysRem = 0;
      return daysRem;
    }
  });

  var avg = getAvg(daysVals);
  var daysRem = {avg: avg, tier: setThreshold(avg, Thresh.daysThresh), unit: 'days'};
  return daysRem;
}

function getVals(meas, prop){
  return meas.reduce((a, i) => {
    return a.concat(i.dat[prop]);
  }, []);
}

function getAvg(arr){
  // remember to convert -1 and -2 for days remaining
  var total = arr.reduce((a, i) => {
    return a += i;
  }, 0);
  return Math.round(total/arr.length);
}

function setThreshold(val, thresh){
  for (let prop in thresh){
    if (thresh[prop].alt && val === thresh[prop].alt) {
      return prop;
    } else if (thresh[prop].min2 && val >= thresh[prop].min2 && val <= thresh[prop].max2) {
      return prop;
    } else if (val >= thresh[prop].min && val <= thresh[prop].max) {
      return prop;
    }
  }
}

function getPercentages(vals, thresh){
  var percs = vals.reduce((a, i) => {
    var tier = setThreshold(i, thresh);
    if (!a[tier]) {
      a[tier] = 1;
    } else {
      a[tier] += 1;
    }
    return a;
  }, {});
  for (let prop in percs) {
    percs[prop] = Math.round((percs[prop]/vals.length) * 100);
  }
  return percs;
}
