
const Vent = require('./schema/Vent');

const AWS = require('aws-sdk');
AWS.config.update({region: process.env.REGION || 'us-west-2'});
const doc = require('dynamodb-doc');
const Dynamo = new doc.DynamoDB();

var SUFFIX = process.env.SUFFIX || '-pilot';


module.exports = async (tms) => {

  var masterTms = tms - 804600;
  // remember when setting on reports tms NOT masterTms

  try {
    var buildings = await getDynamo('Buildings');
    buildings = buildings.Items;
    var devices = await getDynamo('Devices');
    devices = devices.Items;
  } catch(err) {
    return Promise.reject({code: 500, msg: 'Core data not found'});
  }

  var erroredOuts = {
    buildings: [],
    vents: []
  }

  buildings = buildings.filter(b => b.airVents && b.airVents.length > 0);
  var vents = buildings.reduce((a, i) => {
    i.airVents.map(v => {
      v.uId = i.userId;
      v.bId = i.id;
      a.push(new Vent(v));
    })
    return a;
  }, []);

  vents = vents.filter(v => v.currentFilter !== null);
  devices = devices.map(d => {
    var cVent = d.airVentAssignments.find(s => !s.toTimestamp);
    if (cVent === undefined) {
      return null;
    } else {
      return {
        dId: d.deviceId,
        vent: cVent.airVentId,
        installed: cVent.fromTimestamp
      };
    }
  }).filter(dev => dev !== null);

  vents = vents.map(v => {
    v.device = devices.find(d => d.vent === v.vId);
    return v;
  }).filter(v => v.device !== undefined);

  var tArr = [];
  while (tArr.length < 14) {
    tArr.push(tms - (86400 * tArr.length));
  }
  tArr = tArr.reverse().reduce((a, i) => {
    a.push({
      start: i,
      end: i + 86400
    });
    return a;
  }, []);



  vents = await Promise.all(vents.map(v => {
    v = v.getDailyAvgs(tArr);
    return v;
  }));

  vents = vents.map(v => {
    v = v.aggregateWeek();
    return v;
  });

  vents = vents.filter(v => v.week.daysWithMeas > 0);
  Promise.all(vents.map(v => {
    v.userKey = v.uId + '.VENT';
    v.tms = tms;
    Dynamo.putItem({TableName:'Reports'+SUFFIX, Item: v}, (err, data) => {
      if (err) {
        erroredOuts.vents.push(v);
      } else {
        console.log('posted')
        return Promise.resolve(vents);
      }
    });
  }));

};

function getDynamo(table){
  return Dynamo.scan({TableName: table+SUFFIX}).promise();
}
