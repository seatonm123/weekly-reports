
module.exports = {

  daysThresh: {
    good: {
      min: 31,
      max: Infinity,
      alt: -1
    },
    avg: {
      min: 16,
      max: 30
    },
    bad: {
      min: 6,
      max: 15
    },
    crit: {
      min: -2,
      max: 5
    }
  },

  aqThresh: {
    good: {
      min: 0,
      max: 100
    },
    avg: {
      min: 101,
      max: 150
    },
    bad: {
      min: 151,
      max: 300
    },
    crit: {
      min: 301,
      max: 500
    }
  },

  runThresh: {
    good: {
      min: 0,
      max: 25
    },
    avg: {
      min: 26,
      max: 50
    },
    bad: {
      min: 51,
      max: 75
    },
    crit: {
      min: 76,
      max: 100
    }
  },

  tempThresh: {
    good: {
      min: 65,
      max: 75
    },
    avg: {
      min: 50,
      max: 64,
      min2: 76,
      max2: 85
    },
    bad: {
      min: 33,
      max: 49,
      min2: 86,
      max2: 95
    },
    crit: {
      min: -Infinity,
      max: 32,
      min2: 96,
      max2: Infinity
    }
  },
  humThresh: {
    good: {
      min: 35,
      max: 55
    },
    avg: {
      min: 25,
      max: 34,
      min2: 56,
      max2: 65
    },
    bad: {
      min: 10,
      max: 24,
      min2: 66,
      max2: 85
    },
    crit: {
      min: 0,
      max: 9,
      min2: 86,
      max: 100
    }
  }

}
