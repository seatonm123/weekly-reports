
const Core = require('./lib/core');

exports.handler = (event, context, cb) => {

  const response = (code, msg) => {
    if (msg) console.log(JSON.stringify(msg, null, 1));
    cb(null, {
      statusCode: code,
      body: msg ? JSON.stringify(msg) : '',
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
      }
    });
  }

  var now = Math.round(new Date().getTime()/1000);

  Core(now)
    .then(reports => {
      response(200, reports);
    }, err => {
      response(err.code, err.msg);
    });

}
